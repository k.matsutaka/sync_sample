/**
 * Adapterベースクラスの模倣したクラス
 */
export class AdapterBaseDammy {
    /**
     * リクエスト送信を模倣した関数
     * @param url
     * @param request
     * @returns
     */
    protected requestDammy(url: string, request: RequestDammyModel): Promise<ResponseDammyModel> {
        console.log();
        return new Promise((resoleve) => {
            setTimeout(() => {
                const response: ResponseDammyModel = {
                    message: url + "のレスポンス受信"
                }
                resoleve(response);
            }, request.waitTime * 1000);
        });
    }
}

/**
 * Adapterクラスの模倣したクラス
 */
export class AdapterDammy extends AdapterBaseDammy {

    /**
     * リクエスト1
     * @param callBack コールバック関数
     */
    request1(callBack: (res: ResponseDammyModel) => void): void {
        const url: string = "/request1";
        console.log(url + "のリクエスト送信");
        const req: RequestDammyModel = {
            waitTime: 3
        };
        this.requestDammy(url, req).then(res => {
            callBack(res);
        });
    }
    /**
     * リクエスト1'
     * @param callBack コールバック関数
     */
    request1Dash(callBack: (res: ResponseDammyModel) => void): void {
        const url: string = "/request1Dash";
        console.log(url + "のリクエスト送信");
        const req: RequestDammyModel = {
            waitTime: 3
        };
        this.requestDammy(url, req).then(res => {
            callBack(res);
        });
    }

    /**
     * リクエスト2
     * @param callBack コールバック関数
     */
    request2(callBack: (res: ResponseDammyModel) => void): void {
        const url: string = "/request2";
        console.log(url + "のリクエスト送信");
        const req: RequestDammyModel = {
            waitTime: 1
        };
        this.requestDammy(url, req).then(res => {
            callBack(res);
        });
    }
}

/**
 * ダミーリクエスト
 */
export interface RequestDammyModel {
    /** 待機時間 */
    waitTime: number
}

/**
 * ダミーレスポンス
 */
interface ResponseDammyModel {
    /** メッセージ */
    message: string;
}
