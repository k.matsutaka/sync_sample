/**
 * main関数
 * コールバック関数サンプル
 */
function main() {
    const testDataList = MatukenUtil.createTestData();
    // テスト1：チームOHKIのメンバリストを取得
    const test1List = MatukenUtil.extraModelFillter(testDataList, (item) => {
        return item.teamId === "1";
    });
    console.log("テスト1 実行結果 -----");
    console.log(test1List);
    // テスト2：名前が松で始まる人リスト
    const regex = /^松/;
    const test2List = MatukenUtil.extraModelFillter(testDataList, (item) => {
        return regex.test(item.name);
    });
    console.log("テスト2 実行結果 -----");
    console.log(test2List);
}

/** まつけんユーティリティクラス */
class MatukenUtil {

    /**
     * 条件に一致するモデルリストを返却する
     * @param list
     * @param callback
     * @returns
     */
    static extraModelFillter(list: ExtraModel[], callback: (item: ExtraModel) => boolean): ExtraModel[] {
        let retModel: ExtraModel[] = [];
        for (let i = 0; i < list.length; i++) {
            let checkModel = list[i];
            if (callback(checkModel)) {
                retModel.push(checkModel);
            }
        }
        return retModel;
    }

    static createTestData(): ExtraModel[] {
        let retModel: ExtraModel[] = [];
        retModel.push({
            name: "松鷹",
            teamId: "1"
        });
        retModel.push({
            name: "松原",
            teamId: "2"
        });
        retModel.push({
            name: "張",
            teamId: "1"
        });
        retModel.push({
            name: "阪本",
            teamId: "1"
        });
        retModel.push({
            name: "大田",
            teamId: "1"
        });
        retModel.push({
            name: "森本",
            teamId: "1"
        });
        retModel.push({
            name: "史",
            teamId: "1"
        });
        return retModel;
    }
}

/** 番外編モデル */
interface ExtraModel {
    /** 名前 */
    name: string;
    /** TeamId 1:HOKI 2:ENPI */
    teamId: string;
}

// main関数の実行
main();