import { AdapterDammy } from "./adapterDammy";

/**
 * main関数
 * リクエスト1、リクエスト2は非同期
 * 同期処理はそれぞれのコールバック関数内に書く
 */
function main() {
    // AdapterDammyクラスのインスタンス生成
    const adapter: AdapterDammy = new AdapterDammy();

    // リクエスト1実行（処理時間3秒）
    adapter.request1((res) => {
        // リクエスト1のレスポンス受信後の処理
        console.log(res.message);
    });

    //　リクエスト2実行(処理時間1秒)
    adapter.request2((res) => {
        // リクエスト2のレスポンス受信後の処理
        console.log(res.message);
    });
}

// main関数の実行
// 期待値
// 1) リクエスト1開始
// 2) リクエスト2開始
// 3) リクエスト2のレスポンス受信（1秒後）
// 4) リクエスト1のレスポンス受信（3秒後）
main();

