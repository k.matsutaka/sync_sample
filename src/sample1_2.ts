import { AdapterDammy } from "./adapterDammy";

/**
 * main関数
 * リクエスト1のレスポンスを待ち合わせしてから
 * リクエスト1'を実行したい場合
 * ネストして記載するので、可読性が下がる（見ずらい）
 */
function main() {
    // AdapterDammyクラスのインスタンス生成
    const adapter: AdapterDammy = new AdapterDammy();

    // リクエスト1実行（処理時間3秒）
    adapter.request1((res) => {
        // リクエスト1のレスポンス受信
        console.log(res.message);

        // リクエスト1'実行（処理時間3秒）
        adapter.request1Dash((res1Dash) => {
            // リクエスト1'のレスポンス受信
            console.log(res1Dash.message);
        });
    });
}

// main関数の実行
main();